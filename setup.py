from setuptools import setup

setup(
    name='djlib',
    version='0.4.2.1',
    description='',
    author='',
    author_email='',
    url='',
    packages=['djlib'],
    package_data={
        'djlib': [
            'auth/*',
            'console/*',
            'controller/*',
            'db/*.py',
            'db/expression/*',
            'expression/*',
            'templatetags/djlib/*',
            'struct/*',
            'utils/*',
        ]
    },
    install_requires=[
        'django>=1.8.2',
    ]

)
