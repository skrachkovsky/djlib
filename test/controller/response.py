import unittest
from djlib.controller import response


class TestControllerResponseTemplate(unittest.TestCase):
    def test_noparams(self):
        t = response.Template('controller-index.html')
        self.assertEqual(t.getvalue(), b'index content')

    def test_params(self):
        t = response.Template('controller-index-params.html', {'a': 1, 'b': 2.3, 'c': 'abc'})
        self.assertEqual(t.getvalue(), b'index params 1, 2.3, abc')
