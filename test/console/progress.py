from time import sleep

from djlib.console.progress import Bar, Progress


def test_bar1(min_val, max_val, bar_size):
    progress = Bar(min_val=min_val, max_val=max_val, bar_size=bar_size)
    i = 0
    print('Started')
    while not progress.complete:
        i += 1
        progress.update(i)
        sleep(0.5)
    progress.finish()
    print('Finished')


def test_progress1(min_val, max_val, bar_size):
    progress = Progress(min_val=min_val, max_val=max_val, bar_size=bar_size)
    i = 0
    print('Started')
    while not progress.complete:
        i += 1
        progress.update(i)
        sleep(0.5)
    progress.finish()
    print('Finished')
