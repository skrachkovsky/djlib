import unittest
import uuid
from djlib.templatetags.djlib.filter import bin_to_uuid


class TestTemplatetagsFilter(unittest.TestCase):
    def test_bin_to_uuid(self):
        uuid_val = 'f3508c0f-f6ce-11e1-9b3f-002332d62786'
        bin_val = uuid.UUID(uuid_val).bytes
        self.assertEqual(str(bin_to_uuid(bin_val)), uuid_val)
