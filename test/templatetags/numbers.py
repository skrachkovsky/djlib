import unittest
from djlib.templatetags.djlib.numbers import number_format, to_number


class TestTemplatetagsNumbers(unittest.TestCase):
    def test_format1(self):
        num = 1234567.899
        self.assertEqual(number_format(num, '2'), '1,234,567.90')

    def test_format2(self):
        num = 1234567.126
        self.assertEqual(number_format(num, '2, ".", " "'), '1 234 567.13')

    def test_to_number_int(self):
        num = 123
        self.assertEqual(to_number(num), num)

    def test_to_number_float(self):
        num = 123.45
        self.assertEqual(to_number(num), num)
