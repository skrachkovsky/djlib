import unittest
import os

from django.core.management import execute_from_command_line
from test.controller.response import TestControllerResponseTemplate
from test.templatetags.filter import TestTemplatetagsFilter
from test.templatetags.numbers import TestTemplatetagsNumbers

if __name__ == '__main__':
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "test.settings")
    execute_from_command_line([__file__, 'check'])
    unittest.main()
