from django.http.response import HttpResponse
from django.template.response import TemplateResponse
from djlib.utils.json import object_to_json


class Template(TemplateResponse):
    def __init__(self, template: str, params: object=None, request=None, context_instance=None, *args, **kwargs):
        if params is None:
            params = {}
        if context_instance and not request:
            request = context_instance.request
        super().__init__(template=template, request=request, context=params, *args, **kwargs)


class Text(HttpResponse):
    pass


class Json(HttpResponse):
    def __init__(self, content, *args, **kwargs):
        super().__init__(content_type='application/json', content=object_to_json(content), *args, **kwargs)
