from django.utils.decorators import method_decorator


class Bar:
    def __init__(self, min_val=0, max_val=100, bar_size=20, bar_char='#', bar_empty_char=' '):
        if min_val > max_val:
            raise ValueError('Min value cannot be large than Max value')
        self.__min = float(min_val)
        self.__max = float(max_val)
        self.__val = self.__min
        self.__bar_size = abs(int(bar_size))
        self.__bar_char = bar_char
        self.__bar_empty_char = bar_empty_char

    @property
    def complete(self):
        return self.__val == self.__max

    @property
    def min_val(self):
        return self.__min

    @min_val.setter
    def min_val(self, val):
        if float(val) > self.__max:
            raise Exception('Min value cannot be large than Max value')
        self.__min = float(val)

    @property
    def max_val(self):
        return self.__max

    @max_val.setter
    def max_val(self, val):
        if float(val) < self.__min:
            raise Exception('Max value cannot be less than Min value')
        self.__max = float(val)

    def update(self, val):
        if self.__min <= val <= self.__max:
            self.__val = val
            self.repaint()
        elif val < self.__min:
            self.__val = self.__min
        elif val > self.__max:
            self.__val = self.__max

    def repaint(self):
        print('\r', end='')
        self.paint()

    def get_progress(self):
        if self.__min == self.__max:
            return 1
        else:
            return (self.__val - self.__min) / (self.__max - self.__min)

    def paint(self):
        cpl = self.get_progress()
        prog = int(cpl * self.__bar_size + 0.5)
        for p in range(0, self.__bar_size):
            print(self.__bar_char if prog > p else self.__bar_empty_char, end='', flush=True)
        return self

    def finish(self, text=''):
        print('\n' + text if text else '')


def BarWrapper(open_char='[', close_char=']'):
    def _dec(f):
        def wrap(*args, **kwargs):
            print(open_char, end='', flush=True)
            inst = f(*args, **kwargs)
            print(close_char, end='', flush=True)
            return inst
        return wrap

    return _dec


class Percent:
    def __init__(self, f):
        self.__f = f

    def __call__(self, *args, **kwargs):
        inst = self.__f(*args, **kwargs)
        print(' %s%%' % str(int(inst.get_progress() * 100)).rjust(3), end='', flush=True)
        return inst


class Progress(Bar):
    @method_decorator(Percent)
    @method_decorator(BarWrapper())
    def paint(self, *args, **kwargs):
        return super().paint(*args, **kwargs)
