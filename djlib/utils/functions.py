def is_list(inst):
    return inst and (hasattr(inst, '__iter__') or hasattr(inst, '__getitem__')) and not isinstance(inst, str)
