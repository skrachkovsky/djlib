import json


def default(obj):
    if type(obj) not in ['int', 'long', 'float', 'complex', 'str', 'bool']:
        return str(obj)
    return obj


def object_to_json(obj, raise_error=False):
    try:
        return json.dumps(obj, default=default)
    except Exception as exc:
        if raise_error:
            raise exc
    return '[]'
