import json
from django.template import Library

register = Library()


@register.filter(name='number_format')
def number_format_filter(value, args=None):
    try:
        if args:
            args_list = json.loads('[' + str(args) + ']')
        else:
            args_list = []
        args_count = len(args_list)
        decimals = 0
        decimals_point = '.'
        thousands_separator = ','
        if args_count > 0:
            if args_list[0]:
                decimals = int(args_list[0])
            if args_count > 1:
                if args_list[1]:
                    decimals_point = str(args_list[1])
                if args_count > 2:
                    if args_list[2] is not None:
                        thousands_separator = str(args_list[2])
        return number_format(value, decimals, decimals_point, thousands_separator)
    except:
        return ''


def number_format(value, decimals=None, decimals_point=None, thousands_separator=None):
    if decimals is None:
        decimals = 0
    if decimals_point is None:
        decimals_point = '.'
    if thousands_separator is None:
        thousands_separator = ','
    try:
        dec = 'f'
        if decimals == 0:
            dec = 'd'
            value = int(value)
        else:
            dec = '.' + str(decimals) + dec
            value = float(value)
        res = ('{:,' + dec + '}').format(value)
        if decimals_point != '.':
            res = res.replace('.', decimals_point)
        if thousands_separator != ',':
            res = res.replace(',', thousands_separator)

        return res
    except:
        return ''


@register.filter
def plus(value, arg):
    if not isinstance(value, int):
        value = float(value)
    if not isinstance(arg, int):
        arg = float(arg)
    return value + arg


@register.filter
def minus(value, arg):
    if not isinstance(value, int):
        value = float(value)
    if not isinstance(arg, int):
        arg = float(arg)
    return value - arg


@register.filter
def multiply(value, arg):
    if not isinstance(value, int):
        value = float(value)
    if not isinstance(arg, int):
        arg = float(arg)
    return value * arg


@register.filter
def divide(value, arg):
    if not isinstance(value, int):
        value = float(value)
    if not isinstance(arg, int):
        arg = float(arg)
    return value / arg


@register.filter
def to_number(value, arg=None):
    try:
        val = float(value)
        if val == int(val):
            return int(val)
        return val
    except:
        pass
    try:
        return int(value)
    except:
        pass
    if arg is None:
        return 0
    return arg
