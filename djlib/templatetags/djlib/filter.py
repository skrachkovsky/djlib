from binascii import b2a_hex
import uuid
from django.template import Library

register = Library()

@register.filter
def bin_to_uuid(value):
    return uuid.UUID(b2a_hex(value).decode('utf-8'))
