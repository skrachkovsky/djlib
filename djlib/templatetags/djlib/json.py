from django.template.defaulttags import register
from djlib.utils.json import object_to_json


@register.filter
def to_json(obj):
    return object_to_json(obj)
