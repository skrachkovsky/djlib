from django.db import models
from djlib.struct import Object
from copy import copy
from django.utils.translation import get_language


class Translate(models.Model):
    language = models.CharField(max_length=10, db_index=True)

    # parent shout be specified

    class Meta:
        abstract = True
        unique_together = [('language', 'parent')]


class TranslateFacade(models.Model):
    _translate_object = None
    _translate_relation = 'translate'

    def _translate_field(self, name):
        return getattr(self._translate(), name) if self._translate() else None

    def _translate(self):
        if self._translate_object is None:
            language = None
            if isinstance(self, Metadata):
                language = self.get_metadata('language')
            if language is None:
                language = get_language()
            # TODO: all() using for prefetched relations, make different procedures for prefetched and non-prefetched relations
            for t in getattr(self, self._translate_relation).all():
                if t.language == language:
                    self._translate_object = t
                    break
            if self._translate_object is None:
                self._translate_object = False
        return self._translate_object if self._translate_object else None

    # Example usage:

    # @property
    # def name(self):
    #     return self._translate_field('name')

    class Meta:
        abstract = True


class MetadataQueryset(models.QuerySet):
    _metadata = None

    def metadata(self, **kwargs):
        self._metadata = Object(**kwargs)
        return self

    def __iter__(self):
        return self._update_metadata(super().__iter__())

    def _update_metadata(self, res):
        while res:
            r = res.__next__()
            if self._metadata:
                r.metadata = self._metadata
            yield r

    def _clone(self, **kwargs):
        clone = super()._clone(**kwargs)
        clone._metadata = self._metadata
        return clone

    def paste_metadata(self, meta):
        if meta:
            if self._metadata is None:
                self._metadata = Object()
            for key, val in vars(meta).items():
                setattr(self._metadata, key, val)
            self._metadata = meta
        return self


class MetadataManager(models.Manager):
    def get_queryset(self):
        qs = MetadataQueryset(self.model, using=self._db)
        if hasattr(self, '_parent_metadata'):
            qs = qs.paste_metadata(getattr(self, '_parent_metadata'))
        return qs

    def all(self):
        res = super().all()
        if hasattr(self, '_parent_metadata') and self._parent_metadata:
            res = res.paste_metadata(self._parent_metadata)
        return res


class Metadata(models.Model):
    objects = MetadataManager()
    metadata = None

    class Meta:
        abstract = True

    def get_metadata(self, name, default=None):
        if not self.metadata:
            return None
        return getattr(self.metadata, name, default)

    def set_metadata(self, meta):
        self.metadata = meta

    def _copy_metadata(self, meta):
        return copy(meta)

    def __getattribute__(self, item):
        val = super().__getattribute__(item)
        if isinstance(val, models.Model) and isinstance(val, Metadata):
            val.set_metadata(self._copy_metadata(self.metadata))
        elif val.__class__.__name__ in ('RelatedManager', 'ManyRelatedManager') \
                and val.__class__.__module__ == 'django.db.models.fields.related_descriptors' \
                and isinstance(val, MetadataManager):
            setattr(val, '_parent_metadata', self._copy_metadata(self.metadata))
        return val
