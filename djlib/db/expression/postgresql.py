from django.db.models import Aggregate


class ToJson(Aggregate):
    function = 'to_json'
    name = 'ToJson'
    template = '%(function)s(%(expressions)s%(type)s)'

    def __init__(self, expression, type=None, **extra):
        if not expression:
            raise ValueError('Value is empty')
        super().__init__(expression, type='::' + type if type is not None else '', **extra)

    def __repr__(self):
        return "{}({}, type={})".format(
            self.__class__.__name__,
            self.arg_joiner.join(str(arg) for arg in self.source_expressions),
            self.extra['type'][2:] if self.extra['type'] is not None else 'None'
        )


class Coalesce(Aggregate):
    function = 'COALESCE'
    name = 'Coalesce'
    template = '%(function)s(%(expressions)s, %(empty)s)'

    def __init__(self, expression, empty=0, **extra):
        if not expression:
            raise ValueError('Value is empty')
        super().__init__(expression, empty=empty, **extra)

    def __repr__(self):
        return "{}({}, empty={})".format(
            self.__class__.__name__,
            self.arg_joiner.join(str(arg) for arg in self.source_expressions),
            self.extra['empty']
        )
