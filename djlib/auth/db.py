from django.contrib.auth.hashers import make_password, check_password, is_password_usable
from django.db import models
from django.utils.crypto import salted_hmac


class AbstractBaseUser(models.Model):

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.last_login = None
        if update_fields is not None:
            if 'last_login' in update_fields:
                update_fields.remove('last_login')
                if len(update_fields) == 0:
                    return
        super().save(force_insert, force_update, using, update_fields)

    REQUIRED_FIELDS = []

    class Meta:
        abstract = True

    def get_username(self):
        "Return the identifying username for this User"
        return getattr(self, self.USERNAME_FIELD)

    def __str__(self):
        return self.get_username()

    def natural_key(self):
        return (self.get_username(),)

    def is_anonymous(self):
        """
        Always returns False. This is a way of comparing User objects to
        anonymous users.
        """
        return False

    def is_authenticated(self):
        """
        Always return True. This is a way to tell if the user has been
        authenticated in templates.
        """
        return True

    def set_password(self, raw_password):
        setattr(self, self.PASSWORD_FIELD, make_password(raw_password))

    def check_password(self, raw_password):
        """
        Returns a boolean of whether the raw_password was correct. Handles
        hashing formats behind the scenes.
        """
        def setter(raw_password):
            self.set_password(raw_password)
            self.save(update_fields=[self.PASSWORD_FIELD])
        return check_password(raw_password, getattr(self, self.PASSWORD_FIELD), setter)

    def set_unusable_password(self):
        # Sets a value that will never be a valid hash
        setattr(self, self.PASSWORD_FIELD, make_password(None))

    def has_usable_password(self):
        return is_password_usable(getattr(self, self.PASSWORD_FIELD))

    def get_full_name(self):
        raise NotImplementedError('subclasses of AbstractBaseUser must provide a get_full_name() method')

    def get_short_name(self):
        raise NotImplementedError('subclasses of AbstractBaseUser must provide a get_short_name() method.')

    def get_session_auth_hash(self):
        """
        Returns an HMAC of the password field.
        """
        key_salt = "django.contrib.auth.models.AbstractBaseUser.get_session_auth_hash"
        return salted_hmac(key_salt, getattr(self, self.PASSWORD_FIELD)).hexdigest()
